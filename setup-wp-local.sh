#!/bin/bash
#requires wp-cli.yml to be setup

#vagrant side

#delete themes
wp theme delete twentyfourteen twentyfifteen

#rewrite permalinks for pretty urls
wp rewrite structure '/%postname%'  --hard
wp rewrite flush --hard

#install themes
#wp scaffold _s eps --activate --theme_name=EPS --author_uri=http://andregagnon.com --author='Andre Gagnon'
#wp theme activate eps

#set options: turn off comments, set media sizes, uploads folder
wp option update blogdescription 'tagline here'
wp option update default_comment_status closed
wp option update comment_moderation 1
wp option update comment_whitelist ''
wp option update comment_registration 1
wp option update comments_notify 1

wp option update require_name_email ''
wp option update comments_notify 1
wp option update default_ping_status closed
wp option update default_pingback_flag ''
wp option update moderation_notify ''

wp option update show_avatars ''

wp option update thumbnail_size_w 0
wp option update thumbnail_size_h 0
wp option update thumbnail_crop 1
wp option update medium_size_w 0
wp option update medium_size_h 0
wp option update large_size_w 0
wp option update large_size_h 0
wp option update uploads_use_yearmonth_folders ''

#remove admin bar, set screen options
wp user meta update Andy show_admin_bar_front false
# dashboard option
wp user meta update Andy show_welcome_panel 0
# dashboard options, DNW
wp user meta update Andy metaboxhidden_dashboard --format=json '["dashboard_activity","dashboard_quick_press","dashboard_widget","dashboard_primary"]'


#widgets
#wp widget delete search-2
wp widget delete recent-posts-2
wp widget delete recent-comments-2
wp widget delete archives-2
wp widget delete categories-2
wp widget delete meta-2

#install plugins
#wp plugin install custom-post-type-ui --activate
wp plugin install advanced-custom-fields --activate
wp plugin install developer --activate
wp plugin install duplicator # --activate
#wp plugin install bruteprotect # --activate
wp plugin install wordfence # --activate
wp plugin install wordpress-seo # --activate
#wp plugin install the-events-calendar --activate
#wp plugin install woocommerce --activate
#wp plugin install wp-migrate-db # --activate

#wp plugin install https://github.com/afragen/github-updater/archive/master.zip --activate
wp plugin install https://github.com/wp-sync-db/wp-sync-db/archive/master.zip --activate
wp plugin install https://github.com/wp-sync-db/wp-sync-db-media-files/archive/master.zip --activate

#wp hooks plugin
#wp plugin install https://github.com/explodybits/hookr-plugin/archive/master.zip # --activate
#wp plugin install monster-widget # --activate
#wp plugin install custom-contact-forms # --activate
wp plugin delete  hello akismet

#build post type
#wp scaffold post-type video --theme=eps

#change ownership so that backup, updates work with apache
#sudo chown -R  www-data:www-data $1


#delete all pages
wp post delete $(wp post list --post_type=page --format=ids) --force

#build pages: import WXR data e.g. HTML Tag test page
homeID=$(wp post create --post_type=page --post_title='Home' --post_status=publish --porcelain)
#echo "homeID=$homeID"
aboutID=$(wp post create --post_type=page --post_title='About' --post_status=publish --porcelain)
#blogID=$(wp post create --post_type=page --post_title='Blog' --post_status=publish --porcelain)
StyleGuideID=$(wp post create dev/src/HTML-Tags.html --post_type=page --post_title='Style Guide' --post_status=publish --porcelain)
#StyleGuideID=$(wp post --post_type=page --post_title='StyleGuide' --post_status=publish --porcelain)

wp post list --post_type=page --fields=ID,post_title

#build menu, add home page & test-html-tags, assign and activate to primary menu
wp menu create 'Menu 1'
wp menu item add-post menu-1 $homeID
wp menu item add-post menu-1 $aboutID
wp menu item add-post menu-1 $StyleGuideID

wp menu list
wp menu location assign 'Menu 1' primary

#set up default front page, blog page
wp option update show_on_front page
wp option update page_on_front $homeID
#wp option update page_for_posts $blogID




