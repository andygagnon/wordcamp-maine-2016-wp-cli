#!/bin/bash
#Remove WP project in project-folder $1
#get params dbname and directory path from $1
#echo params out to wp-cli.local.yml

if [ "$1" == "" ]
	then
    echo "No arguments provided"
    exit 1
fi

cd ~/Sites/$1

cat > wp-cli.local.yml <<EOF

#temporary wp-cli.local.yml file, deleted at end of script

path: $1

core config:
	dbprefix: wp_$1_
	dbname: dbname

EOF

# view it
cat wp-cli.local.yml

#exit

#remove a wp install
wp db drop

#delete files?
cd $1
sudo rm -rf
cd ~/Sites/$1

#remove temporaray file
rm wp-cli.local.yml






