#!/bin/bash
#build custom taxonomy $3 for post-type $2 in theme $1
#move to WP dev path ~/dev
#requires wp-cli.yml to be setup

if [ "$1" == "" ]
	then
    echo "No arguments provided"
    exit 1
fi

if [ "$2" == "" ]
	then
    echo "No arguments provided"
    exit 1
fi

if [ "$3" == "" ]
	then
    echo "No arguments provided"
    exit 1
fi

#build custom post types $2 for theme $1
wp scaffold taxonomy $3 --post_types=$2 --theme=$1
