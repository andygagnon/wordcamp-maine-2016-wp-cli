#!/bin/bash
#wp-cli script to build a basic wordpress site, using wp-cli.yml file data

#build project $1

if [ "$1" == "" ]
	then
    echo "No project provided"
    exit 1
fi

#if [ "$2" == "" ]
#	then
#   echo "No arguments provided"
#    exit 1
#fi

#vagrant side

#build wp-cli.yml
cat > wp-cli.yml <<EOF

#working directory, relative path
path: $1

#allow .htaccess regenerate
apache_modules:
	- mod_rewrite

#create config file
core config:
	dbprefix: wp_$1_
	dbname: dbname
	dbuser: dbuser
	dbpass: '123'
	extra-php: |
		define( 'WP_POST_REVISIONS', 3);
		define( 'WP_DEBUG', true );
		define( 'SAVEQUERIES', true);
		define('WP_DEBUG_LOG', true);
		define('WP_DEBUG_DISPLAY', false);
		@ini_set('log_errors', 1);
		@ini_set('display_errors', 0);
		@ini_set('error_log', dirname(__FILE__) . '/wp-content/error.log');
		@ini_set( 'error_reporting', E_ALL ^ E_NOTICE );		

#install database
core install:
	title: DEV $1.dev
	url: http://$1.dev
	admin_user: admin
	admin_password: admin
	admin_email: test@test.com

EOF

# view it
cat wp-cli.yml


#vagrant side
#get files, build db
wp core download
wp core config
wp db create
wp core install

#create users
wp user create Andy contact@andregagon.com --user_pass='test' --role=administrator
wp user create Bill bill@andregagon.com --user_pass='test' --role=administrator

#remove admin user 1
wp user delete admin --reassign=Andy




