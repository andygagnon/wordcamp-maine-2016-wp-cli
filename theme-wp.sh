#!/bin/bash
#build theme $2 for site in folder $1 using yoeman, activate
#requires wp-cli.yml to be setup

if [ "$1" == "" ]
	then
    echo "No arguments provided"
    exit 1
fi

if [ "$2" == "" ]
	then
    echo "No arguments provided"
    exit 1
fi

cd ~/Sites/$1
cd $1/wp-content/themes
mkdir $2 && cd $_
yo wp-underscores
cd ~/Sites/$1

#vagrant side
#wp theme activate $2
